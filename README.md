# jf361_ids721_mp7

## Project Introduction
This project involves data processing using a vector database.

## Project Description
- Ingest data into Vector database
- Perform queries and aggregations
- Visualize output

## Project Setup
1. Create a new Rust project.
    ``` 
        cargo new <PROJECT-NAME>
        cd <PROJECT-NAME>
    ```
2. Implement the function in `src/main.rs` and add dependencies in `Cargo.toml`.
3. Use Qdrant for vector database.
    ``` 
        // download Qdrant
        docker pull qdrant/qdrant
   
        // run Qdrant service
        docker run -p 6333:6333 -p 6334:6334 \
            -e QDRANT__SERVICE__GRPC_PORT="6334" \
            qdrant/qdrant
    ```
4. Run the RUST code
    ``` 
        cargo run
    ```

## Project Service
This project provides a query vector-based service that queries the 10 products closest to the vector, prints their information, their average price and displays their distance from the query vector in a chart.

#### Ingest data
Randomly generate 100 products, their prices range from 10 to 1000. Then, ingest them into Vector database.
<p> 
   <img src="screenshot/ingest_code.png" /> 
</p>
Result:
<p> 
   <img src="screenshot/ingest_results.png" /> 
</p>

#### Data query
Randomly generate a vector in `main` and query the product information of the 10 closest to it.
<p> 
   <img src="screenshot/query_code.png" /> 
</p>
Result:
<p> 
   <img src="screenshot/query_results.png" /> 
</p>

#### Data aggregation
Calculate and print the average price of these 10 queried product information.
<p> 
   <img src="screenshot/agg_code.png" /> 
</p>
Result:
<p> 
   <img src="screenshot/agg_results.png" /> 
</p>

#### Visualization
Display the distance of these 10 products from the query vector in the form of a chart.

Results:
<p align="center"> 
   <img src="visual_results.png" /> 
</p>