use anyhow::Result;
use qdrant_client::prelude::*;
use qdrant_client::qdrant::vectors_config::Config;
use qdrant_client::qdrant::{CreateCollection, PointStruct, SearchPoints, SearchResponse, VectorParams, VectorsConfig};
use serde_json::json;
use rand::Rng;
use plotters::prelude::*;


fn generate_random_vector(length: usize) -> Vec<f32> {
    let rng = rand::thread_rng();
    rng.sample_iter(rand::distributions::Standard)
        .take(length)
        .collect()
}


#[tokio::main]
async fn main() -> Result<()> {
    let client = initialize_qdrant_client().await?;

    let collection_name = "product_collection";
    create_collection(&client, collection_name).await?;
    ingest_products(&client, collection_name).await?;

    let query_vector = generate_random_vector(4);
    println!("Query vector: {:?}", query_vector);

    let search_results = query_products(&client, collection_name, query_vector.clone()).await?;
    aggregation(&search_results).await?;
    visualize_results(&search_results)?;

    Ok(())
}


async fn initialize_qdrant_client() -> Result<QdrantClient> {
    let config = QdrantClientConfig::from_url("http://localhost:6334");
    let client = QdrantClient::new(Some(config))?;
    Ok(client)
}


async fn create_collection(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let _ = client.delete_collection(collection_name).await;

    client.create_collection(&CreateCollection {
        collection_name: collection_name.to_string(),
        vectors_config: Some(VectorsConfig {
            config: Some(Config::Params(VectorParams {
                size: 4,
                distance: Distance::Euclid.into(),
                ..Default::default()
            })),
        }),
        ..Default::default()
    }).await?;

    Ok(())
}


async fn ingest_products(client: &QdrantClient, collection_name: &str) -> Result<()> {
    let mut points = Vec::new();
    for i in 0..100 {
        let vector = generate_random_vector(4);
        let payload: Payload = json!({
            "product_id": format!("P{}", i),
            "price": rand::thread_rng().gen_range(10..=1000),
        }).try_into().unwrap();

        points.push(PointStruct::new(i as u64, vector, payload));
    }

    client.upsert_points_blocking(collection_name, None, points, None).await?;
    println!("Products inserted successfully!");
    Ok(())
}


async fn query_products(client: &QdrantClient, collection_name: &str, query_vector: Vec<f32>) -> Result<SearchResponse> {
    let search_points_request = SearchPoints {
        collection_name: collection_name.into(),
        vector: query_vector,
        limit: 10,
        with_payload: Some(true.into()),
        ..Default::default()
    };

    let search_points_response = client.search_points(&search_points_request).await.unwrap();

    println!("Search Results for Products:");
    for (index, point) in search_points_response.result.iter().enumerate() {
        let payload = serde_json::to_string_pretty(&point.payload)
            .unwrap_or_else(|_| "Unable to format payload".to_string());

        println!("Product {}: ", index + 1);
        println!(" - Payload: {}", payload);
        println!(" - Score: {:.2}", point.score);
        println!();

    }
    Ok(search_points_response)
}


async fn aggregation(search_results: &SearchResponse) -> Result<()> {
    let mut total_price = 0.0;
    let mut count = 0;

    for point in &search_results.result {
        let payload = &point.payload;
        let price = payload["price"].as_integer().unwrap() as f64;
            total_price += price;
            count += 1;
    }

    if count > 0 {
        let average_price = total_price / count as f64;
        println!("Average price of the products: ${:.2}", average_price);
    } else {
        println!("No prices found in the search results.");
    }

    Ok(())
}


fn visualize_results(results: &SearchResponse) -> Result<()> {
    let root = BitMapBackend::new("visual_results.png", (640, 480)).into_drawing_area();
    root.fill(&WHITE)?;

    let mut chart = ChartBuilder::on(&root)
        .caption("Query Results", ("sans-serif", 40))
        .margin(5)
        .x_label_area_size(30)
        .y_label_area_size(40)
        .build_cartesian_2d(0..results.result.len(), 0.0..1.0)?;

    chart.configure_mesh().draw()?;

    let data: Vec<(usize, f64)> = results
        .result
        .iter()
        .enumerate()
        .map(|(idx, point)| (idx, point.score as f64))
        .collect();


    chart
        .draw_series(
            data.iter()
                .map(|(x, y)| {
                    let x0 = *x;
                    let x1 = *x + 1;
                    let y0 = 0.0;
                    let y1 = *y;
                    Rectangle::new([(x0, y0), (x1, y1)], BLUE.filled())
                }),
        )?
        .label("Distance")
        .legend(|(x, y)| Rectangle::new([(x, y - 5), (x + 10, y + 5)], BLUE.filled()));

    chart
        .configure_series_labels()
        .background_style(&WHITE.mix(0.8))
        .border_style(&BLACK)
        .draw()?;

    Ok(())
}